# A Hitchhikers guide for CI/CD

This repository contains the different pipeline configurations and snippets from the talk given at the Hackathon X GameJam event at Ulm University.

[[_TOC_]]

## Configuration Overview

- [Getting Started Template by GitLab](./.gitlab-ci.yml)

## What is CI/CD?

## Why is CI/CD useful?

## How can we implement CI/CD measurements?

### GitLab CI

